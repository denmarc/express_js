var express = require('express')
var helmet = require('helmet')
var morgan = require('morgan')
var cors = require('cors')
var bodyParser = require('body-parser')
// const Sequelize  = require('sequelize')
const UserModel = require('./users_model')

// // option 1: passing parameters separately
// const sequelize = new Sequelize('testdatabase','root','',{
//     host:'localhost',
//     dialect: 'mysql' //use 'mysql'
// });

// sequelize
//     .authenticate()
//     .then(()=> {
//         console.log('Connection has been established successfully.');
//     })
//     .catch(err=>{
//         console.error('Unable to connect to the database:',err);
//     });


var app = express()

app.use(helmet())
app.use(morgan('combined'))
app.use(cors())
app.use(bodyParser.json())
app.listen(3005) // port serving

// var get = UserModel.findone()

// respond with "hello world" when a GET request is made to the homepage
app.get('/', async function (req, res) {
   var getAllUsers = await UserModel.findAll()
   if (getAllUsers){
       res.send(getAllUsers)
   }else{
       res.send('Unable to get all users.')
   }
})

app.post('/post',function(req,res){
    console.log(req.body)
   var data ={
       message: 'response from server'
   }
    res.send(data)
})

app.post('/login',async function(req,res){
    var username = req.body.username
    var password = req.body.password

    var checkLogin = await UserModel.findOne({
        where: {
            username: username,
            password: password
        }
    })

    if(checkLogin){
        res.send({
            message: "ok",
            userInfo: checkLogin
        })
    }else{
        res.send({message: "Can't find user"})
    }
})

console.log('My server is running')
