var Sequelize = require('sequelize')


// option 1: passing parameters separately
const sequelize = new Sequelize('testdatabase','root','',{
    host:'localhost',
    dialect: 'mysql' //use 'mysql'
});

sequelize
    .authenticate()
    .then(()=> {
        console.log('Connection has been established successfully.');
    })
    .catch(err=>{
        console.error('Unable to connect to the database:',err);
    });


const Model = Sequelize.Model;

class User extends Model{}
User.init({
    id:{
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    firstName:{
        type: Sequelize.STRING(255),
        allowNull: false
    },
    lastName:{
        type: Sequelize.STRING(255),
        allowNull: false
    },
    gender: {
        type:Sequelize.ENUM ('male','female'),
        allowNull: false
    },
    username:{
        type:Sequelize.STRING(255),
        allowNull: false
    },
    password:{
        type:Sequelize.STRING(255),
        allowNull: false
    }

},{
    sequelize,
    tableName: 'users',
    timestamps: false,

    freezeTableName: true,
    charset: 'utf8mb4'

});
module.exports= User


  